/****************************************************************************
 *        _  ____  _____  ___   _  __  _      ___         __
 *       / |/ / / / / _ \/ _ | / |/ / | | /| / (_)______ / /__ ___ ___
 *      /    / /_/ / , _/ __ |/    /  | |/ |/ / / __/ -_) / -_|_-<(_-<
 *     /_/|_/\____/_/|_/_/ |_/_/|_/   |__/|__/_/_/  \__/_/\__/___/___/
 *
 ************************************************************************//**
 *
 * @file       litecell15.h
 * @brief      Litecell 1.5 system primitive definitions
 * @author     Yves Godin
 *
 * Copyright 2016, NuRAN Wireless Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 ****************************************************************************/
#ifndef LITECELL15_H__
#define LITECELL15_H__

#ifdef __cplusplus
    extern "C" {
#endif // __cplusplus 

/****************************************************************************
 *                                  Includes                                *
 ****************************************************************************/
#include <stdint.h>

#include "gsml1const.h"


/****************************************************************************
 *                                   Const                                  *
 ****************************************************************************/

/**
 * API Version
 */
#define LITECELL15_API_VERSION_MAJOR    2
#define LITECELL15_API_VERSION_MINOR    1
#define LITECELL15_API_VERSION_BUILD    7

#define LITECELL15_API(x,y,z)    ((x << 16) | (y << 8) | z)
#define LITECELL15_API_VERSION   LITECELL15_API(LITECELL15_API_VERSION_MAJOR,LITECELL15_API_VERSION_MINOR,LITECELL15_API_VERSION_BUILD)

 
/****************************************************************************
 * Enum : Litecell15_PrimId_t
 ************************************************************************//**
 *   
 *   System management primitives.
 *
 *   @ingroup litecell15_api
 *   
 ****************************************************************************/
typedef enum Litecell15_PrimId_t
{ 
    Litecell15_PrimId_SystemInfoReq = 0x100,          ///< REQ: System information
    Litecell15_PrimId_SystemInfoCnf,                  ///< CNF: System information
    Litecell15_PrimId_SystemFailureInd,               ///< IND: System failure indication
    Litecell15_PrimId_ActivateRfReq,                  ///< REQ: Activation of the RF section
    Litecell15_PrimId_ActivateRfCnf,                  ///< CNF: Activation of the RF section
    Litecell15_PrimId_DeactivateRfReq,                ///< REQ: Deactivation of the RF section
    Litecell15_PrimId_DeactivateRfCnf,                ///< CNF: Deactivation of the RF section
    Litecell15_PrimId_SetTraceFlagsReq,               ///< REQ: Set the verbosity of the system
    Litecell15_PrimId_Layer1ResetReq,                 ///< REQ: Trigger a reset of the GSM layer 1 library
    Litecell15_PrimId_Layer1ResetCnf,                 ///< CNF: Confirm that the GSM layer 1 library was reset
    Litecell15_PrimId_SetCalibTblReq,                 ///< REQ: Update a calibration table (may be partial)
    Litecell15_PrimId_SetCalibTblCnf,                 ///< CNF: Confirm the update of a calibration table
    Litecell15_PrimId_MuteRfReq,                      ///< REQ: Mute/Unmute the RF section
    Litecell15_PrimId_MuteRfCnf,                      ///< CNF: Confirm the mutin/unmiting of the the RF section
    Litecell15_PrimId_SetRxAttenReq,                  ///< REQ: Set the RX attenuation
    Litecell15_PrimId_SetRxAttenCnf,                  ///< CNF: Confirm the configuration of the RX attenuation
    Litecell15_PrimId_IsAliveReq,                     ///< REQ: Is alive validation
    Litecell15_PrimId_IsAliveCnf,                     ///< CNF: Confirm of is alive validation
    Litecell15_PrimId_SetMaxCellSizeReq,              ///< REQ: Set the maximum cell size
    Litecell15_PrimId_SetMaxCellSizeCnf,              ///< CNF: Confirm the configuration of the maximum cell size
    Litecell15_PrimId_SetC0IdleSlotPowerReductionReq, ///< REQ: Set the C0 power reduction for idle timeslots
    Litecell15_PrimId_SetC0IdleSlotPowerReductionCnf, ///< CNF: Confirm the configuration of the C0 power reduction

    Litecell15_PrimId_NUM = 22

} Litecell15_PrimId_t;

/****************************************************************************
 * typedef : Litecell15_DiversityMode_t
 ************************************************************************//**
 *
 * Diversity mode (if supported by the HW)
 *
 * @ingroup litecell15_api
 *
 ***************************************************************************/
typedef enum GsmL1_DiversityMode
{
    Litecell15_DiversityMode_SISO_A,             ///< SISO path A
    Litecell15_DiversityMode_SISO_B,             ///< SISO path B
    Litecell15_DiversityMode_SIMO_MRC,           ///< Maximum ratio combining (MRC) SIMO

    Litecell15_DiversityMode_NUM                 ///< Only used to validate parameters

} GsmL1_DiversityMode_t;


/****************************************************************************
 *                                  Types                                   *
 ****************************************************************************/

/****************************************************************************
 * Enum: Litecell15_Status_t
 ************************************************************************//**
 *
 * Status used by the Litecell 1.5.
 *
 * @ingroup litecell15_api
 *
 ****************************************************************************/
typedef GsmL1_Status_t Litecell15_Status_t;

/****************************************************************************
 * Struct : Litecell15_SystemInfoCnf_t
 ************************************************************************//**
 * 
 * Structure is used to confirm the information about the system.
 * 
 * @ingroup litecell15_api_prim_sys
 *
****************************************************************************/
typedef struct Litecell15_SystemInfoCnf
{
    Litecell15_Status_t status;       ///< Status
   
    struct
    {
        uint8_t major;              ///< DSP firmware major version number 
        uint8_t minor;              ///< DSP firmware minor version number
        uint8_t build;              ///< DSP firmware build version number
    } dspVersion;

    struct
    {
        uint8_t major;              ///< FPGA firmware major version number 
        uint8_t minor;              ///< FPGA firmware minor version number
        uint8_t build;              ///< FPGA firmware build version number
    } fpgaVersion;
    
    struct
    {
        uint8_t rev;                ///< Board reversion number 
        uint8_t model;              ///< Board option number
    } boardVersion;
    
} Litecell15_SystemInfoCnf_t;
 
/****************************************************************************
 * Struct : Litecell15_SystemFailureInd_t
 ************************************************************************//**
 * 
 * Structure is used to indicate a system failure.
 * 
 * @ingroup litecell15_api_prim_sys
 *
****************************************************************************/
typedef struct Litecell15_SystemFailureInd
{
   Litecell15_Status_t status;          ///< Status

} Litecell15_SystemFailureInd_t;

/****************************************************************************
 * Struct : Litecell15_ActivateRfReq_t
 ************************************************************************//**
 *
 * This primitive is used to request the activation the RF section of the system.
 *
 * @ingroup litecell15_api
 *
 ***************************************************************************/
typedef struct Litecell15_ActivateRfReq
{
    uint8_t u8MaxCellSize;          ///< Maximum cell size in qbits (1 qbit = 138.4 meters, max 166 qbits)
    uint8_t u8UnusedTsMode;         ///< Unused timeslot mode (0:off, 1:min TX power)
    uint8_t u8McCorrMode;           ///< Multi-carrier output level correction mode (0:off, 1:equal power, 2:proportional)
    uint8_t u8DiversityMode;        /**< Diversity mode
                                            0: SISO path A
                                            1: SISO path B
                                            2: Maximum ratio combining (MRC) SIMO */
    struct {
        uint8_t u8UseTchMsgq;       ///< Set to '1' to use a separate MSGQUEUE for TCH primitives
        uint8_t u8UsePdtchMsgq;     ///< Set to '1' to use a separate MSGQUEUE for PDTCH primitives
    } msgq;

    uint8_t u8EnAutoPowerAdjust;    ///< Automatic output power adjustment (0:Disable, 1:Enable)

} Litecell15_ActivateRfReq_t;

/****************************************************************************
 * Struct : Litecell15_ActivateRfCnf_t
 ************************************************************************//**
 *
 * This primitive is used to confirm the activation the RF section of the system.
 *
 * @ingroup litecell15_api
 *
 ***************************************************************************/
typedef struct Litecell15_ActivateRfCnf
{
     Litecell15_Status_t status;      ///< Status
    
} Litecell15_ActivateRfCnf_t;

/****************************************************************************
 * Struct : Litecell15_DeactivateRfCnf_t
 ************************************************************************//**
 *
 * This primitive is used to confirm the deactivation the RF section of the system.
 *
 * @ingroup litecell15_api
 *
 ***************************************************************************/
typedef struct Litecell15_DeactivateRfCnf
{
     Litecell15_Status_t status;      ///< Status
    
} Litecell15_DeactivateRfCnf_t;

/****************************************************************************
 * Struct : Litecell15_SetTraceFlagsReq_t
 ************************************************************************//**
 *
 * This primitive is used to setup the trace flag values. 
 *
 * @ingroup litecell15_api_prim_dbg
 *
 ***************************************************************************/
typedef struct Litecell15_SetTraceFlagsReq
{
    uint32_t u32Tf;                 ///< Trace flag level 
} Litecell15_SetTraceFlagsReq_t;


/****************************************************************************
 * Struct : Litecell15_Layer1ResetReq_t
 ************************************************************************//**
 *
 * Structure is used to request a reset of the layer 1 module.
 *
 * @ingroup litecell15_api_prim_sys
 *
 ****************************************************************************/
typedef struct Litecell15_Layer1ResetReq
{
    uint32_t u32Reserved;

} Litecell15_Layer1ResetReq_t;

/****************************************************************************
 * Struct : Litecell15_Layer1ResetCnf_t
 ************************************************************************//**
 * 
 * Structure is used to confirm the reset of the layer 1 module.
 * 
 * @ingroup litecell15_api_prim_sys
 *
 ****************************************************************************/
typedef struct Litecell15_Layer1ResetCnf
{
    GsmL1_Status_t status;

} Litecell15_Layer1ResetCnf_t;

/****************************************************************************
 * Struct : Litecell15_IsAliveCnf_t
 ************************************************************************//**
 * 
 * Structure is used to confirm the is alive.
 * 
 * @ingroup litecell15_api_prim_sys
 *
 ****************************************************************************/
typedef struct Litecell15_IsAliveCnf
{
    GsmL1_Status_t status;

} Litecell15_IsAliveCnf_t;

/****************************************************************************
 * Struct : Litecell15_SetCalibTblReq_t
 ************************************************************************//**
 *
 * Structure is used to update a calibration table (may be partial).
 *
 * @ingroup litecell15_api_prim_sys
 *
 ****************************************************************************/
typedef struct Litecell15_SetCalibTblReq
{
    uint32_t offset;                ///< Offset of the data
    uint32_t length;                ///< Length of this chunk in bytes
    uint8_t u8Data[1024];           ///< Chuck of the calibration table
} Litecell15_SetCalibTblReq_t;

/****************************************************************************
 * Struct : Litecell15_SetCalibTblCnf_t
 ************************************************************************//**
 *
 * Structure is used to confirm the update of a calibration table
 *
 * @ingroup litecell15_api_prim_sys
 *
 ****************************************************************************/
typedef struct Litecell15_SetCalibTblCnf
{
    uint32_t       offset;          ///< Offset of the data
    uint32_t       length;          ///< Length of this chunk in bytes
    GsmL1_Status_t status;
} Litecell15_SetCalibTblCnf_t;

/****************************************************************************
 * Struct : Litecell15_MuteRfReq_t
 ************************************************************************//**
 *
 * This primitive is used configure if the RF signal should be muted or not
 * for each of the 8 timeslots.
 *
 * @ingroup litecell15_api_prim_dbg
 *
 ***************************************************************************/
typedef struct Litecell15_MuteRfReq
{       
   uint8_t u8Mute[8];            ///< Timeslot mute flag (0:unmute, 1:mute)

} Litecell15_MuteRfReq_t;

/****************************************************************************
 * Struct : Litecell15_MuteRfCnf_t
 ************************************************************************//**
 *
 * This primitive is sent back to confirm the configuration RF mute.
 *
 * @ingroup litecell15_api_prim_dbg
 *
 ***************************************************************************/
typedef struct Litecell15_MuteRfCnf
{        
     Litecell15_Status_t status;   ///< Status of the MUTE-RF-REQ

} Litecell15_MuteRfCnf_t;

/****************************************************************************
 * Struct : Litecell15_SetRxAttenReq_t
 ************************************************************************//**
 *
 * This primitive is used to configure the RF receive attenuation.
 *
 * @ingroup litecell15_api_prim_dbg
 *
 ***************************************************************************/
typedef struct Litecell15_SetRxAttenReq
{
   uint8_t u8Atten;                ///< RX Attenuation: 0(default), 12, 24, 36 dB

} Litecell15_SetRxAttenReq_t;

/****************************************************************************
 * Struct : Litecell15_SetRxAttenCnf_t
 ************************************************************************//**
 *
 * This primitive is sent back to confirm the configuration of the RF receive
 * attenuation.
 *
 * @ingroup litecell15_api_prim_dbg
 *
 ***************************************************************************/
typedef struct Litecell15_SetRxAttenCnf
{
     Litecell15_Status_t status;   ///< Status of the SET-RX-ATTEN-REQ

} Litecell15_SetRxAttenCnf_t;

/****************************************************************************
 * Struct : Litecell15_SetMaxCellSizeReq_t
 ************************************************************************//**
 *
 * This primitive is used to configure the maximum cell size.
 *
 * @ingroup litecell15_api_prim_dbg
 *
 ***************************************************************************/
typedef struct Litecell15_SetMaxCellSizeReq
{
    uint8_t u8MaxCellSize;          ///< Maximum cell size in qbits (1 qbit = 138.4 meters, max 166 qbits)

} Litecell15_SetMaxCellSizeReq_t;

/****************************************************************************
 * Struct : Litecell15_SetMaxCellSizeCnf_t
 ************************************************************************//**
 *
 * This primitive is sent back to confirm the configuration of themaximum
 * cell size.
 *
 * @ingroup litecell15_api_prim_dbg
 *
 ***************************************************************************/
typedef struct Litecell15_SetMaxCellSizeCnf
{
     Litecell15_Status_t status;   ///< Status of the SET-MAXCELLSIZE-REQ

} Litecell15_SetMaxCellSizeCnf_t;

/****************************************************************************
 * Struct : Litecell15_SetC0IdleSlotPowerReductionReq_t
 ************************************************************************//**
 *
 * This primitive is used to configure the C0 idle slot power reduction
 *
 * @ingroup litecell15_api_prim_dbg
 *
 ***************************************************************************/
typedef struct Litecell15_SetC0IdleSlotPowerReductionReq
{
    uint8_t u8PowerReduction;   ///< C0 idle slot power reduction

} Litecell15_SetC0IdleSlotPowerReductionReq_t;

/****************************************************************************
 * Struct : Litecell15_SetC0IdleSlotPowerReductionCnf_t
 ************************************************************************//**
 *
 * This primitive is sent back to confirm the configuration of the
 * C0 idle slot power reduction.
 *
 * @ingroup litecell15_api_prim_dbg
 *
 ***************************************************************************/
typedef struct Litecell15_SetC0IdleSlotPowerReductionCnf
{
    Litecell15_Status_t status;   ///< Status of the SET-C0-IDLE-SLOT-POWER-REDUCTION-REQ

} Litecell15_SetC0IdleSlotPowerReductionCnf_t;

/****************************************************************************
 * Struct : Litecell15_Primt_t
 ************************************************************************//**
 * 
 * System level primitive definition.
 * 
 * @ingroup litecell15_api_prim_sys
 *
 ****************************************************************************/
typedef struct Litecell15_Prim
{
    Litecell15_PrimId_t id;                               ///< Primitive ID

    union 
    {     
        Litecell15_SystemInfoCnf_t                  systemInfoCnf;                  ///< CNF: System information
        Litecell15_SystemFailureInd_t               systemFailureInd;               ///< IND: System failure indication
        Litecell15_ActivateRfReq_t                  activateRfReq;                  ///< REQ: Activation of the RF section
        Litecell15_ActivateRfCnf_t                  activateRfCnf;                  ///< CNF: Activation of the RF section
        Litecell15_DeactivateRfCnf_t                deactivateRfCnf;                ///< CNF: Deactivation of the RF section
        Litecell15_SetTraceFlagsReq_t               setTraceFlagsReq;               ///< REQ: Set the verbosity of the system
        Litecell15_Layer1ResetReq_t                 layer1ResetReq;                 ///< REQ: Trigger a reset of the GSM layer 1 module
        Litecell15_Layer1ResetCnf_t                 layer1ResetCnf;                 ///< CNF: Confirm that the GSM layer 1 module was reset
        Litecell15_SetCalibTblReq_t                 setCalibTblReq;                 ///< REQ: Update a calibration table (may be partial)
        Litecell15_SetCalibTblCnf_t                 setCalibTblCnf;                 ///< CNF: Confirm the update a calibration table (may be partial)
        Litecell15_MuteRfReq_t                      muteRfReq;                      ///< REQ: Mute/Unmute the RF section
        Litecell15_MuteRfCnf_t                      muteRfCnf;                      ///< CNF: Confirm the mutin/unmiting of the the RF section
        Litecell15_SetRxAttenReq_t                  setRxAttenReq;                  ///< REQ: Set the RX attenuation
        Litecell15_SetRxAttenCnf_t                  setRxAttenCnf;                  ///< CNF: Confirm the configuration of the RX attenuation
        Litecell15_IsAliveCnf_t                     isAliveCnf;                     ///< CNF: Confirm that the module is alive
        Litecell15_SetMaxCellSizeReq_t              setMaxCellSizeReq;              ///< REQ: Set the maximum cell size
        Litecell15_SetMaxCellSizeCnf_t              setMaxCellSizeCnf;              ///< CNF: Confirm the maximum cell size
        Litecell15_SetC0IdleSlotPowerReductionReq_t setC0IdleSlotPowerReductionReq; ///< REQ: Set C0 idle slot power reduction
        Litecell15_SetC0IdleSlotPowerReductionCnf_t setC0IdleSlotPowerReductionCnf; ///< CNF: Confirm C0 idle slot power reduction
    } u;

} Litecell15_Prim_t;

 
#ifdef _TMS320C6600
 
/****************************************************************************
 *                               Public Functions                           * 
 ****************************************************************************/

/****************************************************************************
 * Function : Litecell15_RxPrimCallback
 ************************************************************************//**
 * 
 * Litecell15 primitive reception callback routine. Process primitives sent
 * by the ARM processor.
 *
 * @param [in] pPrim
 *      Address of the buffer containing the received primitive.
 *
 * @return
 *      GsmL1_Status_Success or the error code
 *  
 * @ingroup litecell15_api_prim_sys
 *
 ****************************************************************************/ 
void Litecell15_RxPrimCallback( Litecell15_Prim_t *pPrim );
 
 
#endif  // _TMS320C6600

#ifdef __cplusplus
}
#endif  // extern "C"

#endif  // LITECELL15_H__
