/****************************************************************************
 *        _  ____  _____  ___   _  __  _      ___         __
 *       / |/ / / / / _ \/ _ | / |/ / | | /| / (_)______ / /__ ___ ___
 *      /    / /_/ / , _/ __ |/    /  | |/ |/ / / __/ -_) / -_|_-<(_-<
 *     /_/|_/\____/_/|_/_/ |_/_/|_/   |__/|__/_/_/  \__/_/\__/___/___/
 *
 ************************************************************************//**
 *
 * @file       gsml1types.h
 * @brief      GSM Layer 1 Types declarations
 * @author     Yves Godin
 *
 * Copyright 2006, NuRAN Wireless Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 ****************************************************************************/
#ifndef GSML1TYPES_H__
#define GSML1TYPES_H__

#ifdef __cplusplus
    extern "C" {
#endif // __cplusplus 

/****************************************************************************
 *                                  Includes                                *
 ****************************************************************************/

#ifndef WIN32
    #include <stdint.h>
#endif

#include "gsml1const.h"

/****************************************************************************
 *                                  Types                                   *
 ****************************************************************************/

/****************************************************************************
 * typedef : HANDLE
 ************************************************************************//**
 *
 * Generic type used to identify an entity.
 *   
 * @ingroup gsml1_api_types
 *
 ***************************************************************************/
typedef void * HANDLE;

/****************************************************************************
 * typedef : GsmL1_LogChParam_t
 ************************************************************************//**
 *
 * Logical channel configuration parameters
 *
 * @ingroup gsml1_api_prim_params
 *
 ***************************************************************************/
typedef union
{
    // RACH
    struct
    {
        uint8_t u8Bsic;             ///< Base Station Identity Code
    } rach;

    // AGCH
    struct
    {
        uint8_t u8NbrOfAgch;        /**< Number of AGCH blocks on a CCCH
                                        @see BS_AG_BLKS_RES parameter in 3GPP TS 45_02 Section 7 Table 5 */
    } agch;

    // SACCH
    struct
    {
        uint8_t u8MsPowerLevel;     /**< Power level control
                                         @see TS 3GPP 45.005 - 4.1.1 Mobile station output power */
    } sacch;

    // TCH
    struct
    {
        GsmL1_TchPlFmt_t     tchPlFmt;               ///< Payload format (RFC RTP, Etsi IF2)
        GsmL1_TchPlType_t    tchPlType;              ///< Expected payload type (HR, FR, EFR or AMR)
        GsmL1_AmrCmiPhase_t  amrCmiPhase;            ///< AMR Codec Mode Indication phase (downlink)
        GsmL1_AmrCodecMode_t amrInitCodecMode;       ///< AMR Initial Codec Mode
        GsmL1_AmrCodec_t     amrActiveCodecSet[4];   ///< AMR Active Codec Set
    } tch;

    // PRACH
    struct
    {
        uint8_t u8Bsic;             ///< Base Station Identity Code
    } prach;

    // PTCCH
    struct
    {
        uint8_t u8Bsic;             ///< Base Station Identity Code
    } ptcch;

} GsmL1_LogChParam_t;

/****************************************************************************
 * typedef : GsmL1_ConfigParam_t
 ************************************************************************//**
 *
 * Layer 1 configuration parameters
 *
 * @ingroup gsml1_api_prim_params
 *
 ***************************************************************************/
typedef union
{
    // Set the normal burst TSC of the specified RF device
    struct
    {
        uint8_t u8NbTsc;                ///< Normal burst training sequence code of the specified RF device

    } setNbTsc;

    // Set the TX power level of the specified RF device
    struct
    {
        float fTxPowerLevel;            ///< Transmitter power of the specified RF device

    } setTxPowerLevel;

    // Set the logical channel specific parameters
    struct
    {
        GsmL1_Sapi_t       sapi;        ///< Service access point identifier
        uint8_t            u8Tn;        ///< Time-slot [0..7]
        GsmL1_SubCh_t      subCh;       ///< Sub-channel number (0x1F if not applicable)
        GsmL1_Dir_t        dir;         ///< Direction path to configure
        GsmL1_LogChParam_t logChParams; ///< Logical channel parameters

    } setLogChParams;

    ///< Configure the ciphering parameters
    struct 
    {
        uint8_t             u8Tn;       ///< Time-slot [0..7]
        GsmL1_SubCh_t       subCh;      ///< Sub-channel number (0x1F if not applicable)
        GsmL1_Dir_t         dir;        ///< Direction path to configure
        GsmL1_CipherId_t    cipherId;   ///< Ciphering algorithm
        uint8_t             u8Kc[8];    ///< 64-bit ciphering key 
    } setCipheringParams;
    
    ///< Configure the 8-PSK power reduction value
    struct
    {
        uint8_t u8PowerReduction;       ///< 8-PSK power reduction value in dB

    } set8pskPowerReduction;

} GsmL1_ConfigParam_t;
          
#ifdef __cplusplus
}
#endif  // extern "C"

#endif    // GSML1TYPES_H__
