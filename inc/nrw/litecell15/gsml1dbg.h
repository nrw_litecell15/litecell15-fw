/****************************************************************************
 *        _  ____  _____  ___   _  __  _      ___         __
 *       / |/ / / / / _ \/ _ | / |/ / | | /| / (_)______ / /__ ___ ___
 *      /    / /_/ / , _/ __ |/    /  | |/ |/ / / __/ -_) / -_|_-<(_-<
 *     /_/|_/\____/_/|_/_/ |_/_/|_/   |__/|__/_/_/  \__/_/\__/___/___/
 *
 ************************************************************************//**
 *
 * @file       gsml1dbg.h
 * @brief      GSM Layer 1 debug routines
 * @author     Yves Godin
 *
 * Copyright 2006, NuRAN Wireless Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 ****************************************************************************/
#ifndef GSMDBG_H__
#define GSMDBG_H__

#ifdef __cplusplus
    extern "C" {
#endif // __cplusplus 

/****************************************************************************
 *                                 Includes                                 *
 ****************************************************************************/

#include <stdint.h>
#include <stdio.h>

#include "gsml1types.h" // GsmL1_RxBurst_t 

/****************************************************************************
 *                                 Defines                                  *
 ****************************************************************************/

// Debugging system's verbosity level flags
#define DBG_NONE          0x00000000
#define DBG_ALL           0xFFFFFFFF

#define DBG_DEBUG         0x80000000
#define DBG_L1WARNING     0x40000000
#define DBG_ERROR         0x20000000
#define DBG_L1RXMSG       0x10000000

#define DBG_L1RXMSGBYTE   0x08000000
#define DBG_L1TXMSG       0x04000000
#define DBG_L1TXMSGBYTE   0x02000000
#define DBG_MPHCNF        0x01000000

#define DBG_MPHIND        0x00800000
#define DBG_MPHREQ        0x00400000
#define DBG_PHIND         0x00200000
#define DBG_PHREQ         0x00100000

#define DBG_PHYRF         0x00080000
#define DBG_PHYRFMSGBYTE  0x00040000
#define DBG_MODE          0x00020000
#define DBG_TDMAINFO      0x00010000

#define DBG_BADCRC        0x00008000
#define DBG_PHINDBYTE     0x00004000
#define DBG_PHREQBYTE     0x00002000

#define DBG_DEVICEMSG     0x00000800
#define DBG_RACHINFO      0x00000040
#define DBG_LOGCHINFO     0x00000020
#define DBG_MEMORY        0x00000010

#define DBG_PROFILING     0x00000008
#define DBG_TESTCOMMENT   0x00000004
#define DBG_TEST          0x00000002
#define DBG_STATUS        0x00000001

#define DBG_RSSI_THRESHOLD -60.0f ///< Minimum RSSI to be considered a valid burst

/****************************************************************************
 *                              Public functions                            *
 ****************************************************************************/

/****************************************************************************
 * Function : GsmL1Dbg_Init
 ************************************************************************//**
 *
 * Print a debug string to the standard output.
 *
 * @param [in] u32Level
 *    Verbosity level of the printed message.
 *
 * @param [in] pExtPrintfCb
 *    Function pointer to an external routine able to print the formated string.
 *
 * @param [out] pstrOutputFileName
 *    Name of the output file ( Set to NULL in order to disable this feature )
 *
 * @return
 *    None.
 *   
 * @ingroup gsm_common_debug
 *
 ***************************************************************************/
void GsmL1Dbg_Init( uint32_t u32Level, 
                    void (*pExtPrintfCb) ( char *pString), 
                    char *pstrOutputFileName );

/****************************************************************************
 * Function : GsmL1Dbg_SetLevel
 ************************************************************************//**
 *
 * Set the verbosity level.
 *
 * @param [in] u32Level
 *    Verbosity level of the printed message.
 *
 * @return
 *    None.
 *   
 * @ingroup gsm_common_debug
 *
 ***************************************************************************/
void GsmL1Dbg_SetLevel( uint32_t u32Level );               
               
/****************************************************************************
 * Function : GsmL1Dbg_GetLevel
 ************************************************************************//**
 *
 * Get the current verbosity level.
 *
 * @param
 *    None.
 *
 * @return
 *    Current verbosity level.
 *   
 * @ingroup gsm_common_debug
 *
 ***************************************************************************/
uint32_t GsmL1Dbg_GetLevel( void );

/****************************************************************************
 * Function : GsmL1Dbg_Close
 ************************************************************************//**
 *
 * Close the debugging system.
 *
 * @return
 *    None.
 *   
 * @ingroup gsm_common_debug
 *
 ***************************************************************************/
void GsmL1Dbg_Close();

/****************************************************************************
 * Function : GsmL1Dbg_Printf
 ************************************************************************//**
 *
 * Print a debug string to the standard output.
 *
 * @param [in] u32Level
 *    Verbosity level of the printed message.
 *
 * @param [in] *format
 *    String that contains the text to be written to the stdout.
 *
 * @return
 *    None.
 *   
 * @ingroup gsm_common_debug
 *
 ***************************************************************************/
void GsmL1Dbg_Printf( uint32_t u32Level, char const *format, ... );

/****************************************************************************
 * Function : GsmL1Dbg_DumpBuffer
 ************************************************************************//**
 *
 * Dump a generic uint8_t buffer.
 *
 * @param [in] u32Level
 *    Verbosity level of the printed message.
 *
 * @param [in] u32FirstByte
 *    Starting byte index
 * 
 * @param [in] u32Length
 *    Number of bytes to dump
 *
 * @param [in] pu8Buffer
 *    Pointer to the dumped buffer
 *
 * @return
 *    None.
 *   
 * @ingroup gsm_common_debug
 *
 ***************************************************************************/
void GsmL1Dbg_DumpBuffer( uint32_t u32Level, 
                          uint32_t u32FirstByte, 
                          uint32_t u32Length, 
                          uint8_t *pu8Buffer );

#ifdef __cplusplus
}
#endif  // extern "C"

#endif	// GSMDBG_H__
