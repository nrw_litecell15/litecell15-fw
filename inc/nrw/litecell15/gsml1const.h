/****************************************************************************
 *        _  ____  _____  ___   _  __  _      ___         __
 *       / |/ / / / / _ \/ _ | / |/ / | | /| / (_)______ / /__ ___ ___
 *      /    / /_/ / , _/ __ |/    /  | |/ |/ / / __/ -_) / -_|_-<(_-<
 *     /_/|_/\____/_/|_/_/ |_/_/|_/   |__/|__/_/_/  \__/_/\__/___/___/
 *
 ************************************************************************//**
 *
 * @file       gsml1const.h
 * @brief      GSM Layer 1 API constant definitions.
 * @author     Yves Godin
 *
 * Copyright 2006, NuRAN Wireless Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 ****************************************************************************/
#ifndef GSML1CONST_H__
#define GSML1CONST_H__

#ifdef __cplusplus
    extern "C" {
#endif // __cplusplus 


/****************************************************************************
 *                               Public macro                               *
 ****************************************************************************/

/****************************************************************************
 * Macro : CALL_GSML1
 ************************************************************************//**
 *
 * This macro calls a GsmL1 function and jump to the gsml1_error label if the
 * returned value is different from GsmL1_Status_Success. 
 *
 * @note A gsml1_error label must be defined in the function.
 *
 * @ingroup gsml1_api_const
 *
 ***************************************************************************/
#define CALL_GSML1(x) if( ( status = x ) != GsmL1_Status_Success ) goto gsml1_error;

/****************************************************************************
 *                             Public constants                             *
 ****************************************************************************/

/****************************************************************************
 * Enum: : GsmL1_SubCh_t
 ************************************************************************//**
 *
 * GSM Layer 1 sub-channel identifiers.
 *   
 * @ingroup gsml1_api_const
 *
 ***************************************************************************/
typedef enum GsmL1_SubCh_t
{    
    GsmL1_SubCh_0 = 0,      ///< Sub-channel 0
    GsmL1_SubCh_1,          ///< Sub-channel 1
    GsmL1_SubCh_2,          ///< Sub-channel 2
    GsmL1_SubCh_3,          ///< Sub-channel 3
    GsmL1_SubCh_4,          ///< Sub-channel 4
    GsmL1_SubCh_5,          ///< Sub-channel 5
    GsmL1_SubCh_6,          ///< Sub-channel 6
    GsmL1_SubCh_7,          ///< Sub-channel 7
    GsmL1_SubCh_8,          ///< Sub-channel 8
    GsmL1_SubCh_9,          ///< Sub-channel 9
    GsmL1_SubCh_10,         ///< Sub-channel 10
    GsmL1_SubCh_11,         ///< Sub-channel 11
    GsmL1_SubCh_12,         ///< Sub-channel 12
    GsmL1_SubCh_13,         ///< Sub-channel 13
    GsmL1_SubCh_14,         ///< Sub-channel 14
    GsmL1_SubCh_15,         ///< Sub-channel 15
    
    GsmL1_SubCh_NA = 0x1F   ///< No sub-channel (not applicable)

} GsmL1_SubCh_t;
 
/****************************************************************************
 * typedef : GsmL1_Sapi_t
 ************************************************************************//**
 *
 * Service Access Point Identifier (SAPI).
 *   
 * An SAPI is used to identifie a specific Service Access Point (SAP)
 *
 * @ingroup gsml1_api_const
 *
 ***************************************************************************/
typedef enum GsmL1_Sapi_t
{    
    GsmL1_Sapi_Idle = 0,    ///< Idle burst pseudo-channel                   

    GsmL1_Sapi_Fcch,        ///< Frequency correction burst pseudo-channel         
    GsmL1_Sapi_Sch,         ///< Synchronization channel                     
    GsmL1_Sapi_Sacch,       ///< Slow associated control channel        
    GsmL1_Sapi_Sdcch,       ///< Stand-alone dedicated control channel        
    GsmL1_Sapi_Bcch,        ///< Broadcast control channel        
    GsmL1_Sapi_Pch,         ///< Paging channel        
    GsmL1_Sapi_Agch,        ///< Access grant channel        
    GsmL1_Sapi_Cbch,        ///< Cell Broadcast Channel
    GsmL1_Sapi_Rach,        ///< Random access channel              
    GsmL1_Sapi_TchF,        ///< Full rate speech traffic channel                     
    GsmL1_Sapi_FacchF,      ///< Fast associated control channel at full rate
    GsmL1_Sapi_TchH,        ///< Half rate speech traffic channel
    GsmL1_Sapi_FacchH,      ///< Fast associated control channel at half rate
    GsmL1_Sapi_Nch,         ///< Notification Channel
    GsmL1_Sapi_Pdtch,       ///< Packet data traffic channel
    GsmL1_Sapi_Pacch,       ///< Packet associated control channel
    GsmL1_Sapi_Pbcch,       ///< Packet broadcast control channel
    GsmL1_Sapi_Pagch,       ///< Packet access grant channel
    GsmL1_Sapi_Ppch,        ///< Packet paging channel
    GsmL1_Sapi_Pnch,        ///< Packet notification channel        
    GsmL1_Sapi_Ptcch,       ///< Packet timing advance control channel
    GsmL1_Sapi_Prach,       ///< Packet random access channel            
             
    GsmL1_Sapi_NUM          ///< Only used to retrieve the last SAPI number

} GsmL1_Sapi_t;

/****************************************************************************
 * Enum: GsmL1_Status_t
 ************************************************************************//** 
 * 
 * Status used by layer 1 functions.
 * 
 * @ingroup gsml1_api_const
 *
 ****************************************************************************/
typedef enum GsmL1_Status_t
{
    GsmL1_Status_Success            =   0,  ///< No error          

    GsmL1_Status_Generic            =  -1,  ///< Generic error
    GsmL1_Status_NoMemory           =  -2,  ///< Not enough memory error 
    GsmL1_Status_Timeout            =  -3,  ///< Timeout error
    GsmL1_Status_InvalidParam       =  -4,  ///< Invalid parameter error
    GsmL1_Status_Busy               =  -5,  ///< Busy ressource error
    GsmL1_Status_NoRessource        =  -6,  ///< No more ressource available error
    GsmL1_Status_Uninitialized      =  -7,  ///< Trying to use an unitialized resource
    GsmL1_Status_NullInterface      =  -8,  ///< Trying to call a NULL interface
    GsmL1_Status_NullFctnPtr        =  -9,  ///< Trying to call a NULL function ptr
    GsmL1_Status_BadCrc             =  -10, ///< Bad CRC error
    GsmL1_Status_BadUsf             =  -11, ///< Bad USF error
    GsmL1_Status_InvalidCPS         =  -12, ///< Invalid CPS field
    GsmL1_Status_UnexpectedBurst    =  -13, ///< Unexpected Burst
    GsmL1_Status_UnavailCodec       =  -14, ///< AMR codec is unavailable
    GsmL1_Status_CriticalError      =  -15, ///< Critical error
    GsmL1_Status_OverheatError      =  -16, ///< Overheat error
    GsmL1_Status_DeviceError        =  -17, ///< Device error
    GsmL1_Status_FacchError         =  -18, /**< Trying to activate a Facch prior activating a Tch logCh or
                                                 Trying to deactivate a Tch logCh prior deactivating all active Facch blocks */
    GsmL1_Status_AlreadyDeactivated =  -19, ///< Trying to deactivate an already deactivated LogCh
    GsmL1_Status_TxBurstFifoOvrn    =  -20, ///< Trying to write into a Full Fifo
    GsmL1_Status_TxBurstFifoUndr    =  -21, ///< Fifo underrun
    GsmL1_Status_NotSynchronized    =  -22, ///< Not synchronized
    GsmL1_Status_Unsupported        =  -23, ///< Unsupported feature
    GsmL1_Status_ClockError         =  -24, ///< System clock error

    GsmL1_Status_LogRfMaxError      =  -25, ///< Log detector reached the maximum error to correct

    GSML1_STATUS_NUM                =   26

} GsmL1_Status_t;

/****************************************************************************
 * Enum: GsmL1_LogChComb_t
 ************************************************************************//** 
 * 
 * Permitted logical channel combinations over a physical channel.
 *
 * @ingroup gsml1_api_const
 *
 ****************************************************************************/
typedef enum GsmL1_LogChComb_t
{
    GsmL1_LogChComb_0 = 0,   ///< UNUSED (DUMMY BURST) 
    GsmL1_LogChComb_I,       ///< TCH/F + FACCH/F + SACCH/TF  
    GsmL1_LogChComb_II,      ///< TCH/H(0,1) + FACCH/H(0,1) + SACCH/TH(0,1)      
    GsmL1_LogChComb_IV,      ///< FCCH + SCH + BCCH + CCCH  
    GsmL1_LogChComb_V,       ///< FCCH + SCH + BCCH + CCCH + SDCCH/4(0..3) + SACCH/C4(0..3)  
    GsmL1_LogChComb_VII,     ///< SDCCH/8(0 .7) + SACCH/C8(0 . 7)   
    GsmL1_LogChComb_XIII     ///< PDTCH/F + PACCH/F + PTCCH/F  
        
} GsmL1_LogChComb_t;                                

/****************************************************************************
 * Enum : GsmL1_DevType_t
 ************************************************************************//**
 *   
 *   PHY device type.
 *
 *   @ingroup gsml1_api_const
 *   
 ****************************************************************************/
typedef enum GsmL1_DevType_t
{
    GsmL1_DevType_TxdRxu = 0x06,    ///< Base station type (TX downlink, RX uplink)
    GsmL1_DevType_TxuRxd = 0x09,    ///< Mobile station type (TX uplink, RX downlink)
    GsmL1_DevType_Rxd    = 0x01,    ///< Monitor type (RX downlink only)
    GsmL1_DevType_Rxu    = 0x02,    ///< Monitor type (RX uplink only)
    GsmL1_DevType_Txd    = 0x04,    ///< Jammer type (TX downlink only)
    GsmL1_DevType_Txu    = 0x08,    ///< Jammer type (TX uplink only)
    GsmL1_DevType_TxuRxu = 0x0A     ///< Loopback type (TX uplink with downlink frame format, Rx uplink)

} GsmL1_DevType_t;

/****************************************************************************
 * Enum : GsmL1_Dir_t
 ************************************************************************//**
 *   
 *   Indicate the path of the transmission.
 *
 *   @ingroup gsml1_api_const
 *   
 ****************************************************************************/
typedef enum GsmL1_Dir_t
{
    GsmL1_Dir_None       = 0x00,    ///< No path
    GsmL1_Dir_RxDownlink = 0x01,    ///< Receive transmission path from Bts to the mobile station
    GsmL1_Dir_RxUplink   = 0x02,    ///< Receive transmission path from mobile station to the Bts
    GsmL1_Dir_TxDownlink = 0x04,    ///< Transmit on transmission path from Bts to the mobile station
    GsmL1_Dir_TxUplink   = 0x08     ///< Transmit on transmission path from mobile station to the Bts

} GsmL1_Dir_t;

/****************************************************************************
 * Enum : GsmL1_CipherId_t
 ************************************************************************//**
 *   
 *   Ciphering algorithm ID.
 *
 *   @ingroup gsml1_api_const
 *   
 ****************************************************************************/
typedef enum GsmL1_CipherId_t
{
    GsmL1_CipherId_A50  = 0x00,      ///< No encryption
    GsmL1_CipherId_A51  = 0x01,      ///< A5/1
    GsmL1_CipherId_A52  = 0x02,      ///< A5/2
    GsmL1_CipherId_A53  = 0x03       ///< A5/3

} GsmL1_CipherId_t;

/****************************************************************************
 * Enum : GsmL1_FreqBand_t
 ************************************************************************//**
 *   
 *   GSM frequency bands.
 *
 *   @ingroup gsml1_api_const
 *   
 ****************************************************************************/
typedef enum GsmL1_FreqBand_t
{
    GsmL1_FreqBand_850 = 0,  ///< GSM-850 frequency band
    GsmL1_FreqBand_900,      ///< GSM-900 frequency band    
    GsmL1_FreqBand_1800,     ///< GSM-1800 frequency band    
    GsmL1_FreqBand_1900,     ///< GSM-1900 frequency band

    Num_GsmL1_FreqBand       ///< Number of frequency bands

} GsmL1_FreqBand_t;

/****************************************************************************
 * Enum : GsmL1_TchFormat_t
 ************************************************************************//**
 *   
 *   TCH message packin format.
 *
 *   @ingroup gsml1_api_const
 *   
 ****************************************************************************/
typedef enum GsmL1_TchFormat_t
{
    GsmL1_TchFormat_Rtp = 0,    ///< RTP format
    GsmL1_TchFormat_If2         ///< IF2 format (AMR), custom (HR, FR, EFR)

} GsmL1_TchFormat_t;

/****************************************************************************
 * Enum : GsmL1_PrimId_t
 ************************************************************************//**
 *   
 *   Inter-layer communication primitives ID definitions
 *
 *   @ingroup gsml1_api_const
 *   
 ****************************************************************************/
typedef enum GsmL1_PrimId_t
{ 
    GsmL1_PrimId_MphInitReq = 0,    ///< Initialize the base station. 
    GsmL1_PrimId_MphCloseReq,       ///< Closes the base station. 
    GsmL1_PrimId_MphConnectReq,     ///< Connect a physical channel. 
    GsmL1_PrimId_MphDisconnectReq,  ///< Disconnect a physical channel. 
    GsmL1_PrimId_MphActivateReq,    ///< Activate a logical channel over a physical channel. 
    GsmL1_PrimId_MphDeactivateReq,  ///< Deactivate a logical channel from a physical channel. 
    GsmL1_PrimId_MphConfigReq,      ///< Configure a physical channel. 
	GsmL1_PrimId_MphMeasureReq,     ///< Measure the received signal strenght indication on a device.
    GsmL1_PrimId_MphInitCnf,        ///< Confirm the reception of a MPH-INIT_REQ primitive. 
    GsmL1_PrimId_MphCloseCnf,       ///< Confirm the reception of a MPH-CLOSE_REQ primitive. 
    GsmL1_PrimId_MphConnectCnf,     ///< Returns an handle pointing to the connected physical channel. 
    GsmL1_PrimId_MphDisconnectCnf,  ///< Confirm the reception of a MPH-DISCONNECT_REQ primitive. 
    GsmL1_PrimId_MphActivateCnf,    ///< Confirm the reception of a MPH-ACTIVATE_REQ primitive. 
    GsmL1_PrimId_MphDeactivateCnf,  ///< Confirm the reception of a MPH-DEACTIVATE_REQ primitive. 
    GsmL1_PrimId_MphConfigCnf,      ///< Confirm the reception of a MPH-CONFIG_REQ primitive. 
	GsmL1_PrimId_MphMeasureCnf,     ///< Returns the measurement made on the reception of a MPH-MEASURE-REQ primitive.
    GsmL1_PrimId_MphTimeInd,        ///< Infom upper layer of the actual timeslot and frame number. 
    GsmL1_PrimId_MphSyncInd,        ///< Synchronization status indication
    GsmL1_PrimId_PhEmptyFrameReq,   ///< Layer 2 has nothing to transmit on a PH-READY-TO-SEND. 
    GsmL1_PrimId_PhDataReq,         ///< Transfer a message frame unit to the layer 1. 
    GsmL1_PrimId_PhConnectInd,      ///< Inform layer 2 that a logical channel has connected on layer 1. 
    GsmL1_PrimId_PhReadyToSendInd,  ///< Ask layer 3 for something to transmit. 
    GsmL1_PrimId_PhDataInd,         ///< Transmit a message frame unit to layer 2. 
    GsmL1_PrimId_PhRaInd,           ///< Inform the layer 2 of a random access initiated by the MS. 

    GsmL1_PrimId_NUM                ///< Only used to validate parameters

} GsmL1_PrimId_t;

/****************************************************************************
 * enum: GsmL1_BurstType_t
 ************************************************************************//**
 *
 * GSM Burst Types
 *   
 * @see
 *  TS 3GPP 45.002 - 5.0 
 *
 * @ingroup gsml1_common_const
 *
 ***************************************************************************/
typedef enum GsmL1_BurstType_t
{
    GsmL1_BurstType_Sync       = 0,  ///< Synchronization Burst
    GsmL1_BurstType_Normal     = 1,  ///< Normal Burst
    GsmL1_BurstType_Dummy      = 2,  ///< Dummy Burst
    GsmL1_BurstType_Normal8Psk = 3,  ///< Normal Burst (8PSK)    
    GsmL1_BurstType_Access_0   = 4,  ///< Access Burst (Training sequence 0) 
    GsmL1_BurstType_Access_1   = 5,  ///< Access Burst (Training sequence 1)
    GsmL1_BurstType_Access_2   = 6,  ///< Access Burst (Training sequence 2)
    GsmL1_BurstType_Freq       = 7,  ///< Frequency Correction Burst
    
    GsmL1_BurstType_NUM              ///< Only used to validate parameters

} GsmL1_BurstType_t;

/****************************************************************************
 * typedef enum : GsmL1_If2FrTypeIdx_t
 ************************************************************************//**
 *
 * List of every IF2 frame types.
 *   
 * @ingroup gsml1_api_const
 *
 ***************************************************************************/
typedef enum GsmL1_If2FrTypeIdx_t
{
    GsmL1_If2FrTypeIdx_4_75 = 0,    ///< AMR 4,75
    GsmL1_If2FrTypeIdx_5_15,        ///< AMR 5,15
    GsmL1_If2FrTypeIdx_5_9,         ///< AMR 5,9
    GsmL1_If2FrTypeIdx_6_7,         ///< AMR 6,7
    GsmL1_If2FrTypeIdx_7_4,         ///< AMR 7,4
    GsmL1_If2FrTypeIdx_7_95,        ///< AMR 7,95
    GsmL1_If2FrTypeIdx_10_2,        ///< AMR 10,2
    GsmL1_If2FrTypeIdx_12_2,        ///< AMR 12,2
    GsmL1_If2FrTypeIdx_SID,         ///< AMR SID
    GsmL1_If2FrTypeIdx_NoData = 15  ///< NO DATA

} GsmL1_If2FrTypeIdx_t;

/****************************************************************************
 * typedef enum : GsmL1_TchPlType_t
 ************************************************************************//**
 *
 * List of every payload type supported on a TCH physical channel.
 *   
 * @ingroup gsml1_api_const
 *
 ***************************************************************************/
typedef enum GsmL1_TchPlType_t
{   
    GsmL1_TchPlType_NA = 0,             ///< Not applicable, unknown, invalid

    GsmL1_TchPlType_Fr,                 ///< Compacted ETSI full-rate traffic speech frame
    GsmL1_TchPlType_Hr,                 ///< Compacted ETSI half-rate traffic speech frame
    GsmL1_TchPlType_Efr,                ///< Compacted ETSI enhanced full-rate traffic speech frame
    GsmL1_TchPlType_Amr,                ///< RTP/IF2 adaptive multi-rate traffic speech frame

    // These cannot be formatted using IF2 format !!
    GsmL1_TchPlType_Amr_SidBad,         ///< Indicates that errors are detected
    GsmL1_TchPlType_Amr_Onset,          ///< Used to signal the Codec mode for the first speech frame after DTX
    GsmL1_TchPlType_Amr_Ratscch,        ///< Frames used to convey RATSCCH messages

    GsmL1_TchPlType_Amr_SidUpdateInH,   ///< Used to inhibit the second part of a SID_UPDATE frame if there is a speech onset
    GsmL1_TchPlType_Amr_SidFirstP1,     ///< First part of marker to define end of speech, start of DTX
    GsmL1_TchPlType_Amr_SidFirstP2,     ///< Second part of marker to define end of speech, start of DTX
    GsmL1_TchPlType_Amr_SidFirstInH,    ///< Used to inhibit the second part of a SID_FIRST_P1 frame if there is a speech onset
    GsmL1_TchPlType_Amr_RatscchMarker,  ///< Marker to identify Ratscch frames
    GsmL1_TchPlType_Amr_RatscchData     ///< Frame that conveys the actual Ratscch message
    
} GsmL1_TchPlType_t;

/****************************************************************************
 * Enum : GsmL1_TchPlFmt_t
 ************************************************************************//**
 *   
 *   List of every payload packing format supported on a TCH physical channel.
 *   
 *   @ingroup gsml1_api_const
 *   
 ****************************************************************************/
typedef enum GsmL1_TchPlFmt_t
{
    GsmL1_TchPlFmt_Rtp = 0,     ///< RFC XXXX - RTP format
    GsmL1_TchPlFmt_If2          ///< Etsi IF2 format (AMR), custom (HR, FR, EFR)

} GsmL1_TchPlFmt_t;

/****************************************************************************
 * typedef enum : GsmL1_PdtchPlType_t
 ************************************************************************//**
 *
 * List of every payload type supported on a PDTCH physical channel.
 *   
 * @ingroup gsml1_api_const
 *
 ***************************************************************************/
typedef enum GsmL1_PdtchPlType_t
{   
    GsmL1_PdtchPlType_NA        = 0x00,    ///< Not applicable, unknown, invalid
    GsmL1_PdtchPlType_Hdr       = 0x01,    ///< Header only is present and valid
    GsmL1_PdtchPlType_Hdr_Data1 = 0x03,    ///< Header and 1st half of data (2nd half is present but invalid)
    GsmL1_PdtchPlType_Hdr_Data2 = 0x05,    ///< Header and 2nd half of data (1st half is present but invalid)
    GsmL1_PdtchPlType_Full      = 0x07     ///< Full data present and valid    
   
} GsmL1_PdtchPlType_t;

/****************************************************************************
 * typedef enum : GsmL1_AmrCodec_t
 ************************************************************************//**
 *
 * List of every supported codecs of a TCH/AMR active codec set.
 *   
 * @ingroup gsml1_api_const
 *
 ***************************************************************************/
typedef enum GsmL1_AmrCodec_t 
{
    GsmL1_AmrCodec_Unset = 0,  ///< Used to indicate that a slot of the Active Codec Slot is unset 

    GsmL1_AmrCodec_4_75,       ///< 4,75 Kbits/s codec
    GsmL1_AmrCodec_5_15,       ///< 5,15 Kbits/s codec
    GsmL1_AmrCodec_5_9,        ///< 5,9  Kbits/s codec
    GsmL1_AmrCodec_6_7,        ///< 6,7  Kbits/s codec
    GsmL1_AmrCodec_7_4,        ///< 7,4  Kbits/s codec
    GsmL1_AmrCodec_7_95,       ///< 7,95 Kbits/s codec
    GsmL1_AmrCodec_10_2,       ///< 10,2 Kbits/s codec
    GsmL1_AmrCodec_12_2,       ///< 12,2 Kbits/s codec
    
    GsmL1_AmrCodec_NUM         ///< Only used to retrieve the last Codec number

} GsmL1_AmrCodec_t;

/****************************************************************************
 * typedef enum : GsmL1_AmrCmiPhase_t
 ************************************************************************//**
 *
 * Available codec mode indication phase.
 *   
 * @ingroup gsml1_api_const
 *
 ***************************************************************************/
typedef enum GsmL1_AmrCmiPhase_t 
{
    GsmL1_AmrCmiPhase_NA = 0,   ///< AMR CMI phase is not applicable
    GsmL1_AmrCmiPhase_Odd,      ///< AMR odd phase codec mode indication (default)
    GsmL1_AmrCmiPhase_Even      ///< AMR even phase codec mode indication
    
} GsmL1_AmrCmiPhase_t;

/****************************************************************************
 * typedef enum : GsmL1_AmrCodecMode_t
 ************************************************************************//**
 *
 * Available codec modes of a TCH/AFS or TCH/AHS active codec set.
 *   
 * @ingroup gsml1_api_const
 *
 ***************************************************************************/
typedef enum GsmL1_AmrCodecMode_t 
{
    GsmL1_AmrCodecMode_1 = 0, ///< AMR Codec Mode 1
    GsmL1_AmrCodecMode_2,     ///< AMR Codec Mode 2
    GsmL1_AmrCodecMode_3,     ///< AMR Codec Mode 3
    GsmL1_AmrCodecMode_4,     ///< AMR Codec Mode 4
    GsmL1_AmrCodecMode_Unset  ///< Unset AMR Codec Mode

} GsmL1_AmrCodecMode_t;

/****************************************************************************
 * typedef : GsmL1_ConfigParamId_t
 ************************************************************************//**
 *
 * Id used to identify which parameter is configured through a MPH-CONFIG-REQ.
 *   
 * @ingroup gsml1_api_const
 *
 ***************************************************************************/
typedef enum GsmL1_ConfigParamId_t
{
    GsmL1_ConfigParamId_SetNbTsc,              ///< Set the normal burst TSC of the specified RF device
    GsmL1_ConfigParamId_SetTxPowerLevel,       ///< Set the TX power level of the specified RF device
    GsmL1_ConfigParamId_SetLogChParams,        ///< Configure the logical channel specific parameters
    GsmL1_ConfigParamId_SetCipheringParams,    ///< Configure the ciphering parameters
    GsmL1_ConfigParamId_Set8pskPowerReduction, ///< Configure 8-PSK power reduction value

    GsmL1_ConfigParamId_NUM                    ///< Only used to validate parameters

} GsmL1_ConfigParamId_t;


/****************************************************************************
 *                               Public constant                            *
 ****************************************************************************/

/// Maximum length of a layer 2 message unit.
#define MAX_L2MSGSIZE     160 // ( Based on a PDTCH block type 13 (MCS-9) [155 bytes] )

/// Advance taken by the library (counted in frames where 1 frame = 8 timeslots )
#define GSML1_FN_ADVANCE  6

///< Size (in byte) of a Traffic Channel header located at the start of every msg unit
#define TCH_HEADER_SIZE   1

#ifdef __cplusplus
}
#endif  // extern "C"

#endif	// GSML1CONST_H__

